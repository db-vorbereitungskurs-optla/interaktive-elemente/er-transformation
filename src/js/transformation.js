const $ = require("jquery");

let transformation = null;
transformation = transformation || {};

transformation.schrittnamen = [
    '1. Tabellen erstellen',
    '2. Attribute eintragen',
    '3. Primärschlüssel kennzeichnen',
    '4. Fremdschlüssel kennzeichnen 1:N',
    '5. Fremdschlüssel kennzeichnen N:M',
    '6. Beziehung mit Beziehungsattribut',
    '7. Datentypen bestimmen',
    'Fertig!',
];

transformation.schrittinhalte = new Array(8);
transformation.schrittverlauf = "0";

transformation.fuelleschritte = function() {
    for (let i = 0; i <= 8; i++) {
        transformation.schrittinhalte[i] = new Array(2);
        transformation.schrittinhalte[i][0] = 'stufe' + i;
        transformation.schrittinhalte[i][1] = 'none';
    }
};

$(document).ready(() => {
    transformation.fuelleschritte();

    let seite = document.getElementById('myRange');
    if (seite) {
        seite = seite.value;
    } else {
        return;
    }

    for (let i = 1; i <= 8; i++) {
        let elem = document.getElementById(transformation.schrittinhalte[i][0]);
        if (elem) {
            elem.style.display = transformation.schrittinhalte[i][1];
        }
    }

    $('#links').on('click', function() {
        transformation.schrittzurueck();
    });

    $('#rechts').on('click', function() {
        transformation.schrittvor();
    });

    $('#entity-lehrer').on('click', function() {
        transformation.tab_lehrerein();
    });

    $('#entity-klasse').on('click', function() {
        transformation.tab_klasseein();
    });

    $('#attr_wochenstunden').on('click', function() {
        transformation.at_wochenstundenein();
    });

    $('#attr_teilzeit').on('click', function() {
        transformation.at_teilzeitein();
    });

    $('#attr_geburtsdatum').on('click', function() {
        transformation.at_geburtsdatumein();
    });

    $('#attr_name').on('click', function() {
        transformation.at_nameein();
    });

    $('#attr_name2').on('click', function() {
        transformation.at_snameein();
    });

    $('#attr_schuelernrein').on('click', function() {
        transformation.at_schuelernrein();
    });

    $('#attr_elternsprecher').on('click', function() {
        transformation.at_elternsprecherein();
    });

    $('#attr_fach').on('click', function() {
        transformation.at_fachein();
    });

    $('#attribut-klasse').on('click', function() {
        transformation.at_klasseein();
    });

    $('#attr_klassenlehrer').on('click', function() {
        transformation.at_klassenlehrerein();
    });

    $('#entity-schueler').on('click', function() {
        transformation.tab_schuelerein();
    });

    $('#attr_name').on('click', function() {
        transformation.at_nameein();
    });

    $('#attr_personalausweis').on('click', function() {
        transformation.at_personalnrein();
    });

    $('#attr_schueler').on('click', function() {
        transformation.at_schuelernrein();
    });

    $('#rel-lehrt').on('click', function() {
        transformation.tab_lehrtein();
    });

    $('#zeige-datentypen').on('click', function() {
        transformation.datentypenein();
    });

    $('#loesung').on('click', function() {
        transformation.setCurrentValue("7");
        transformation.sichtbarkeit();
        document.getElementById('anzeige').innerHTML = transformation.schrittnamen["7"];
        transformation.anzeigeeinaus();
    });

    $('#myRange').on('change', function(e) {
        transformation.sichtbarkeit();
        transformation.anzeigeeinaus();
        document.getElementById('anzeige').innerHTML = transformation.schrittnamen[e.target.value];
    });
});

transformation.tab_schuelerein = function() {
    const wert = transformation.getCurrentValue();
    if (wert === '0') {
        document.getElementById('tab_schueler').style.visibility = 'visible';
    }
};

transformation.tab_lehrerein = function() {
    const wert = transformation.getCurrentValue();
    if (wert === '0') {
        document.getElementById('tab_lehrer').style.visibility = 'visible';
    }
};

transformation.tab_klasseein = function() {
    const wert = transformation.getCurrentValue();
    if (wert === '0') {
        document.getElementById('tab_klasse').style.visibility = 'visible';
    }
};

transformation.at_klasseein = function() {
    const wert = transformation.getCurrentValue();

    if (wert === '1') {
        document.getElementById('at_klasse').style.visibility = 'visible';
    }

    if (wert === '2') {
        document.getElementById('prim_klasse').style.visibility = 'visible';
    }

    if (wert === '3') {
        document.getElementById('fremd_klasse').style.visibility = 'visible';
        document.getElementById('fremd_line1').style.visibility = 'visible';
        document.getElementById('rel3a').style.visibility = 'visible';
        document.getElementById('rel3b').style.visibility = 'visible';
    }

    if (wert === '4') {
        document.getElementById('fremd_klasse2').style.visibility = 'visible';
        document.getElementById('prim_fremd_klasse2').style.visibility = 'visible';
        document.getElementById('rel2a').style.visibility = 'visible';
        document.getElementById('rel2b').style.visibility = 'visible';
        document.getElementById('fremd_line3').style.visibility = 'visible';
    }
};

transformation.at_teilzeitein = function() {
    const wert = transformation.getCurrentValue();
    if (wert === '1') {
        document.getElementById('at_teilzeit').style.visibility = 'visible';
    }
};

transformation.at_personalnrein = function() {
    const wert = transformation.getCurrentValue();

    if (wert === '1') {
        document.getElementById('at_personalnr').style.visibility = 'visible';
    }
    if (wert === '2') {
        document.getElementById('prim_personalnr').style.visibility = 'visible';
    }
    if (wert === '4') {
        document.getElementById('fremd_personalnr').style.visibility = 'visible';
        document.getElementById('prim_fremd_personalnr').style.visibility = 'visible';
        document.getElementById('fremd_line2').style.visibility = 'visible';
        document.getElementById('rel1a').style.visibility = 'visible';
        document.getElementById('rel1b').style.visibility = 'visible';
    }
};

transformation.at_nameein = function() {
    const wert = transformation.getCurrentValue();
    if (wert === "1") {
        document.getElementById('at_fname').style.visibility = 'visible';
        document.getElementById('at_vname').style.visibility = 'visible';
    }
};

transformation.at_snameein = function() {
    const wert = transformation.getCurrentValue();
    if (wert === "1") {
        document.getElementById('at_sfname').style.visibility = 'visible';
        document.getElementById('at_svname').style.visibility = 'visible';
    }
};

transformation.at_elternsprecherein = function() {
    wert = transformation.getCurrentValue();
    if (wert === "1") {
        document.getElementById('at_elternsprecher').style.visibility = 'visible';
    }
};

transformation.at_klassenlehrerein = function() {
    const wert = transformation.getCurrentValue();
    if (wert === "1") {
        document.getElementById('at_klassenlehrer').style.visibility = 'visible';
    }
};

transformation.at_schuelernrein = function() {
    const wert = document.getElementById('myRange').value;
    if (wert === "1") {
        document.getElementById('at_schuelernr').style.visibility = 'visible';
    }
    if (wert === "2") {
        document.getElementById('prim_schuelernr').style.visibility = 'visible';
    }
};

transformation.at_geburtsdatumein = function() {
    const wert = document.getElementById('myRange').value;
    if (wert === "1") {
        document.getElementById('at_geburtsdatum').style.visibility = 'visible';
    }
};

transformation.tab_lehrtein = function() {
    const wert = document.getElementById('myRange').value;
    if (wert === "4") {
        document.getElementById('tab_lehrt').style.visibility = 'visible';
    }
};

transformation.at_wochenstundenein = function() {
    const wert = document.getElementById('myRange').value;
    if (wert === "5") {
        document.getElementById('at_wochenstunden').style.visibility = 'visible';
    }
};

transformation.at_fachein = function() {
    const wert = document.getElementById('myRange').value;
    if (wert === "5") {
        document.getElementById('at_fach').style.visibility = 'visible';
    }
};

transformation.schrittkontrolle = function() {
    const wert = document.getElementById('myRange').value;
    wertkontrolle = transformation.schrittverlauf.search(wert);
    if (wertkontrolle === (-1)) {
        vorwertkontrolle = transformation.schrittverlauf.search(wert - 1);
        if (vorwertkontrolle === (-1)) {
            document.getElementById('myRange').value = transformation.schrittverlauf.charAt(transformation.schrittverlauf.length - 1);
            // document.getElementById('anzeige').innerHTML = transformation.schrittnamen [schrittverlauf.charAt(schrittverlauf.length - 1)];
            transformation.anzeigeeinaus();
        } else {
            transformation.schrittverlauf += wert;
        }
    }
};

transformation.anzeigeeinaus = function() {
    const n = transformation.getCurrentValue();
    let i = 0;

    for (i = 0; i <= 7; i++) {
        transformation.schrittinhalte[i][1] = 'none';
    }

    switch (n) {
        case '0':
            transformation.schrittinhalte[0][1] = 'block';
            break;
        case '1':
            transformation.schrittinhalte[1][1] = 'block';
            break;
        case '2':
            transformation.schrittinhalte[2][1] = 'block';
            break;
        case '3':
            transformation.schrittinhalte[3][1] = 'block';
            break;
        case '4':
            transformation.schrittinhalte[4][1] = 'block';
            break;
        case '5':
            transformation.schrittinhalte[5][1] = 'block';
            break;
        case '6':
            transformation.schrittinhalte[6][1] = 'block';
            break;
        case '7':
            transformation.schrittinhalte[7][1] = 'block';
            break;
    }

    for (i = 0; i <= 7; i++) {
        document.getElementById(transformation.schrittinhalte[i][0]).style.display = transformation.schrittinhalte[i][1];
    }
};


transformation.schrittvor = function() {
    const oldValue = transformation.getCurrentValue();
    let schritt = parseInt(oldValue, 10);
    schritt += 1;
    if (schritt > 7) {
        schritt = 7;
    }

    document.getElementById('myRange').value = schritt;
    document.getElementById('anzeige').innerHTML = transformation.schrittnamen[schritt];
    transformation.sichtbarkeit();
    transformation.anzeigeeinaus();
};

transformation.schrittzurueck = function() {
    const oldValue = transformation.getCurrentValue();
    let schritt = parseInt(oldValue, 10);
    schritt -= 1;
    if (schritt <= 0) {
        schritt = 0;
    }
    document.getElementById('myRange').value = schritt;
    document.getElementById('anzeige').innerHTML = transformation.schrittnamen[schritt];
    transformation.sichtbarkeit();
    transformation.anzeigeeinaus();
};

transformation.tabellen = ['tab_lehrer', 'tab_klasse', 'tab_schueler'];
transformation.attribute = ['at_klasse', 'at_teilzeit', 'at_personalnr', 'at_fname', 'at_vname',
    'at_sfname', 'at_schuelernr', 'at_geburtsdatum', 'at_svname', 'at_elternsprecher', 'at_klassenlehrer',
];
transformation.prim = ['prim_klasse', 'prim_personalnr', 'prim_schuelernr'];
transformation.fremd = ['fremd_klasse', 'fremd_line1', 'rel3a', 'rel3b'];
transformation.rTabelle = ['tab_lehrt'];
transformation.rAttribute = ['at_wochenstunden', 'at_fach'];
transformation.rFremd = ['fremd_personalnr', 'fremd_klasse2', 'fremd_line2', 'fremd_line3',
    'prim_fremd_personalnr', 'prim_fremd_klasse2',
    'rel1a', 'rel1b', 'rel2b', 'rel2a',
];
transformation.typ = ['dat_personalnr', 'dat_fach', 'dat_fremdpersonalnr',
    'dat_personalnr', 'dat_geburtsdatum', 'dat_vname', 'dat_fname', 'dat_svname', 'dat_sfname',
    'dat_teilzeit', 'dat_elternsprecher', 'dat_klassenlehrer', 'dat_wochenstunden', 'dat_klasse',
    'dat_schuelernr', 'dat_fremdklasse', 'dat_fremdklasse2',
];

transformation.getCurrentValue = function() {
    return document.getElementById('myRange').value;
};

transformation.setCurrentValue = function(wert) {
    document.getElementById('myRange').value = wert;
};

transformation.sichtbarkeit = function() {
    const wert = transformation.getCurrentValue();
    let transSichtbar = [];
    let transVersteckt = [];
    switch (wert) {
        case '0':
            transVersteckt = transformation.attribute
                .concat(transformation.tabellen)
                .concat(transformation.prim)
                .concat(transformation.fremd)
                .concat(transformation.rTabelle)
                .concat(transformation.rFremd)
                .concat(transformation.rAttribute)
                .concat(transformation.typ);
            break;
        case '1':
            transSichtbar = transformation.tabellen;
            transVersteckt = transformation.attribute
                .concat(transformation.prim)
                .concat(transformation.fremd)
                .concat(transformation.rTabelle)
                .concat(transformation.rFremd)
                .concat(transformation.rAttribute)
                .concat(transformation.typ);
            break;
        case '2':
            transSichtbar = transformation.tabellen
                .concat(transformation.attribute);
            transVersteckt = transformation.prim
                .concat(transformation.fremd)
                .concat(transformation.rTabelle)
                .concat(transformation.rFremd)
                .concat(transformation.rAttribute)
                .concat(transformation.typ);
            break;
        case '3':
            transSichtbar = transformation.tabellen
                .concat(transformation.attribute)
                .concat(transformation.prim);
            transVersteckt = transformation.fremd
                .concat(transformation.rTabelle)
                .concat(transformation.rFremd)
                .concat(transformation.rAttribute)
                .concat(transformation.typ);
            break;
        case '4':
            transSichtbar = transformation.tabellen
                .concat(transformation.attribute)
                .concat(transformation.prim)
                .concat(transformation.fremd);
            transVersteckt = transformation.rTabelle
                .concat(transformation.rFremd)
                .concat(transformation.rAttribute)
                .concat(transformation.typ);
            break;
        case '5':
            transSichtbar = transformation.tabellen
                .concat(transformation.attribute)
                .concat(transformation.prim)
                .concat(transformation.fremd)
                .concat(transformation.rTabelle)
                .concat(transformation.rFremd);
            transVersteckt = transformation.rAttribute
                .concat(transformation.typ);
            break;
        case '6':
            transSichtbar = transformation.tabellen
                .concat(transformation.attribute)
                .concat(transformation.prim)
                .concat(transformation.fremd)
                .concat(transformation.rTabelle)
                .concat(transformation.rFremd)
                .concat(transformation.rAttribute);
            transVersteckt = transformation.typ;
            break;
        case '7':
            transSichtbar = transformation.tabellen
                .concat(transformation.attribute)
                .concat(transformation.prim)
                .concat(transformation.fremd)
                .concat(transformation.rTabelle)
                .concat(transformation.rFremd)
                .concat(transformation.rAttribute)
                .concat(transformation.typ);
            break;
        default:
            transformationsListe = transformation.tabellen;
    }
    transSichtbar.forEach(elem => document.getElementById(elem).style.visibility = "visible");
    transVersteckt.forEach(elem => document.getElementById(elem).style.visibility = "hidden");
};

transformation.datentypenein = function() {
    transformation.typ.forEach(elem => document.getElementById(elem).style.visibility = "visible");
    transformation.setCurrentValue("7");
    document.getElementById('anzeige').innerHTML = transformation.schrittnamen["7"];
    transformation.anzeigeeinaus();
};
