const $ = require("jquery");

const assignments = {
    0:{"entity_museum":"tab_museum", "entity_art":"tab_art"},
    1:{"attribut_museum_number":"tab_attr_museum_number", "attr_title":"tab_attr_title", "attr_address":"tab_attr_address",
       "attr_art_number":"tab_attr_art_number", "attr_artist":["tab_attr_artist_name","tab_attr_artist_surname"], "attr_art_title":"tab_attr_art_title"},
    2:{"attribut_museum_number":"prim_museum_number", "attr_art_number":"prim_art_number"},
    3:{"attribut_museum_number":"tab_attr_fremd_museum_number"}
}

const datatypes = ["dat_museum_number","dat_title", "dat_address", "dat_art_number", "dat_fremd_museum_number", "dat_art_title", "dat_artist_name", "dat_artist_surname"];

let current_step = 0;
let itemCount = {};

$(document).ready(() => {

    initItemCount();

    $("#svg_elements").children().on('click', function() {
        clickedId = $(this).attr("id");
        if (clickedId in assignments[current_step]) {
            let objectId = assignments[current_step][clickedId];
            let countArray = itemCount[current_step];
            if (!countArray.includes(objectId)) {
                countArray.push(objectId);
                if(Array.isArray(objectId)){
                    objectId.forEach(item => $("#"+item).show());
                } else {
                    $("#"+objectId).show();
                }
                if (countArray.length == Object.keys(assignments[current_step]).length) {
                    showNextStep(current_step);
                }
            }
        }
        
    });

    $('#show_types').on('click', function() {
        if (current_step == 4) {
            datatypes.forEach(item => $("#"+item).show());
            showNextStep(current_step);
        }
    });

    $('#reset').on('click', function() {
        for (let i = 0; i <= current_step; i++) {
            $.map(assignments[i], function(value, key) { 
                if(Array.isArray(value)){
                    value.forEach(item => $("#"+item).hide());
                } else {
                    $("#"+value).hide();
                }
            });
        }
        datatypes.forEach(item => $("#"+item).hide());
        showNextStep(-1);
        initItemCount();
    });

    function showNextStep(currentStep) {
        $("#stufe"+current_step).hide();
        current_step = currentStep + 1;
        $("#stufe"+current_step).show();
    }

    function initItemCount() {
        for (let i = 0; i <= 5; i++) {
            itemCount[i] = [];
        }
    }
});