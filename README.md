# ER-Transformation

Dieses Tool veranschaulicht das Überführen eines ER-Modells in Datenbanktabellen.


## Systemvorraussetzungen
Node.js v16 (gallium) muss installiert sein.

## Installation der Pakete des Repositories
Navigiere auf einer Kommandozeile in den Ordner, in dem sich die Datei package.json befindet.

Mit
```bash
yarn
```
bzw. 
```bash
npm install
```
können hier die Pakete, die in der Datei package.json gelistet sind, installiert werden.

## Entwicklungsumgebung
Mit
```bash
source start.sh
```
kann die Entwicklungskonfiguration von webpack mit der zugehörigen Umgebung gestart gestartet werden. Es öffnet sich ein Browserfenster und unter der URL http://localhost:9000 erscheint die lokale Version der Webseite.

## Produktiv-Test
Mit
```bash
yarn run start
```
werden die Dateien analog zu einem Produktivsystem gebaut und lokal verfügbar gemacht.


